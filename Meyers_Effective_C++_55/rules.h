#ifndef MEYERS55_RULES_H_
#define MEYERS55_RULES_H_

#include "debug_print.h"
#include "const_and_non_const_func.h"
#include "ref_to_static.h"
#include "virtual_constructor_destructor.h"
#include "assignment.h"
#include "destr_in_smart_ptr.h"
#include "explicit_and_default_value.h"
#include "specific_swap.h"
#include "pure_vitrual_with_reales.h"
#include "using_declaration.h"
#include "rules35_function.h"
#include "default_value.h"
#include "nonexplicit_interface.h"
#include "dependent_template_type_name.h"
#include "traits.h"
#include "catch_new.h"
#include "new_with_params.h"

#endif /* MEYERS55_RULES_H_ */
